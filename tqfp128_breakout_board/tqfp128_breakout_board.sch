EESchema Schematic File Version 4
LIBS:tqfp128_breakout_board-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L TQFP_BREAKOUT_BOARD:TQFP_BREAKOUT_BOARD U1
U 1 1 5CD2F02F
P 700 5400
F 0 "U1" H 650 5400 50  0000 L CNN
F 1 "TQFP_BREAKOUT_BOARD" H 250 5300 50  0000 L CNN
F 2 "own:Breakout_board_tqfp128" H 700 5400 50  0000 C CNN
F 3 "" H 700 5400 50  0001 C CNN
	1    700  5400
	1    0    0    -1  
$EndComp
Text GLabel -1650 3900 0    50   Input ~ 0
1
Text GLabel -1650 4000 0    50   Input ~ 0
2
Text GLabel -1650 4100 0    50   Input ~ 0
3
Text GLabel -1650 4200 0    50   Input ~ 0
4
Text GLabel -1650 4300 0    50   Input ~ 0
5
Text GLabel -1650 4400 0    50   Input ~ 0
6
Text GLabel -1650 4500 0    50   Input ~ 0
7
Text GLabel -1650 4600 0    50   Input ~ 0
8
Text GLabel -1650 4700 0    50   Input ~ 0
9
Text GLabel -1650 4800 0    50   Input ~ 0
10
Text GLabel -1650 4900 0    50   Input ~ 0
11
Text GLabel -1650 5000 0    50   Input ~ 0
12
Text GLabel -1650 5100 0    50   Input ~ 0
13
Text GLabel -1650 5200 0    50   Input ~ 0
14
Text GLabel -1650 5300 0    50   Input ~ 0
15
Text GLabel -1650 5400 0    50   Input ~ 0
16
Text GLabel -1650 5500 0    50   Input ~ 0
17
Text GLabel -1650 5600 0    50   Input ~ 0
18
Text GLabel -1650 5700 0    50   Input ~ 0
19
Text GLabel -1650 5800 0    50   Input ~ 0
20
Text GLabel -1650 5900 0    50   Input ~ 0
21
Text GLabel -1650 6000 0    50   Input ~ 0
22
Text GLabel -1650 6100 0    50   Input ~ 0
23
Text GLabel -1650 6200 0    50   Input ~ 0
24
Text GLabel -1650 6300 0    50   Input ~ 0
25
Text GLabel -1650 6400 0    50   Input ~ 0
26
Text GLabel -1650 6500 0    50   Input ~ 0
27
Text GLabel -1650 6600 0    50   Input ~ 0
28
Text GLabel -1650 6700 0    50   Input ~ 0
29
Text GLabel -1650 6800 0    50   Input ~ 0
30
Text GLabel -1650 6900 0    50   Input ~ 0
31
Text GLabel -1650 7000 0    50   Input ~ 0
32
Text GLabel 2250 7500 3    50   Input ~ 0
64
Text GLabel 2150 7500 3    50   Input ~ 0
63
Text GLabel 2050 7500 3    50   Input ~ 0
62
Text GLabel 1950 7500 3    50   Input ~ 0
61
Text GLabel 1850 7500 3    50   Input ~ 0
60
Text GLabel 1750 7500 3    50   Input ~ 0
59
Text GLabel 1650 7500 3    50   Input ~ 0
58
Text GLabel 1550 7500 3    50   Input ~ 0
57
Text GLabel 1450 7500 3    50   Input ~ 0
56
Text GLabel 1350 7500 3    50   Input ~ 0
55
Text GLabel 1250 7500 3    50   Input ~ 0
54
Text GLabel 1150 7500 3    50   Input ~ 0
53
Text GLabel 1050 7500 3    50   Input ~ 0
52
Text GLabel 950  7500 3    50   Input ~ 0
51
Text GLabel 850  7500 3    50   Input ~ 0
50
Text GLabel 750  7500 3    50   Input ~ 0
49
Text GLabel 650  7500 3    50   Input ~ 0
48
Text GLabel 550  7500 3    50   Input ~ 0
47
Text GLabel 450  7500 3    50   Input ~ 0
46
Text GLabel 350  7500 3    50   Input ~ 0
45
Text GLabel 250  7500 3    50   Input ~ 0
44
Text GLabel 150  7500 3    50   Input ~ 0
43
Text GLabel 50   7500 3    50   Input ~ 0
42
Text GLabel -50  7500 3    50   Input ~ 0
41
Text GLabel -150 7500 3    50   Input ~ 0
40
Text GLabel -250 7500 3    50   Input ~ 0
39
Text GLabel -350 7500 3    50   Input ~ 0
38
Text GLabel -450 7500 3    50   Input ~ 0
37
Text GLabel -550 7500 3    50   Input ~ 0
36
Text GLabel -650 7500 3    50   Input ~ 0
35
Text GLabel -750 7500 3    50   Input ~ 0
34
Text GLabel -850 7500 3    50   Input ~ 0
33
Text GLabel 3050 6950 2    50   Input ~ 0
65
Text GLabel 3050 6850 2    50   Input ~ 0
66
Text GLabel 3050 6750 2    50   Input ~ 0
67
Text GLabel 3050 6650 2    50   Input ~ 0
68
Text GLabel 3050 6550 2    50   Input ~ 0
69
Text GLabel 3050 6450 2    50   Input ~ 0
70
Text GLabel 3050 6350 2    50   Input ~ 0
71
Text GLabel 3050 6250 2    50   Input ~ 0
72
Text GLabel 3050 6150 2    50   Input ~ 0
73
Text GLabel 3050 6050 2    50   Input ~ 0
74
Text GLabel 3050 5950 2    50   Input ~ 0
75
Text GLabel 3050 5850 2    50   Input ~ 0
76
Text GLabel 3050 5750 2    50   Input ~ 0
77
Text GLabel 3050 5650 2    50   Input ~ 0
78
Text GLabel 3050 5550 2    50   Input ~ 0
79
Text GLabel 3050 5450 2    50   Input ~ 0
80
Text GLabel 3050 5350 2    50   Input ~ 0
81
Text GLabel 3050 5250 2    50   Input ~ 0
82
Text GLabel 3050 5150 2    50   Input ~ 0
83
Text GLabel 3050 5050 2    50   Input ~ 0
84
Text GLabel 3050 4950 2    50   Input ~ 0
85
Text GLabel 3050 4850 2    50   Input ~ 0
86
Text GLabel 3050 4750 2    50   Input ~ 0
87
Text GLabel 3050 4650 2    50   Input ~ 0
88
Text GLabel 3050 4550 2    50   Input ~ 0
89
Text GLabel 3050 4450 2    50   Input ~ 0
90
Text GLabel 3050 4350 2    50   Input ~ 0
91
Text GLabel 3050 4250 2    50   Input ~ 0
92
Text GLabel 3050 4150 2    50   Input ~ 0
93
Text GLabel 3050 4050 2    50   Input ~ 0
94
Text GLabel 3050 3950 2    50   Input ~ 0
95
Text GLabel 3050 3850 2    50   Input ~ 0
96
Text GLabel -850 3350 1    50   Input ~ 0
128
Text GLabel -750 3350 1    50   Input ~ 0
127
Text GLabel -650 3350 1    50   Input ~ 0
126
Text GLabel -550 3350 1    50   Input ~ 0
125
Text GLabel -450 3350 1    50   Input ~ 0
124
Text GLabel -350 3350 1    50   Input ~ 0
123
Text GLabel -250 3350 1    50   Input ~ 0
122
Text GLabel -150 3350 1    50   Input ~ 0
121
Text GLabel -50  3350 1    50   Input ~ 0
120
Text GLabel 50   3350 1    50   Input ~ 0
119
Text GLabel 150  3350 1    50   Input ~ 0
118
Text GLabel 250  3350 1    50   Input ~ 0
117
Text GLabel 350  3350 1    50   Input ~ 0
116
Text GLabel 450  3350 1    50   Input ~ 0
115
Text GLabel 550  3350 1    50   Input ~ 0
114
Text GLabel 650  3350 1    50   Input ~ 0
113
Text GLabel 750  3350 1    50   Input ~ 0
112
Text GLabel 850  3350 1    50   Input ~ 0
111
Text GLabel 950  3350 1    50   Input ~ 0
110
Text GLabel 1050 3350 1    50   Input ~ 0
109
Text GLabel 1150 3350 1    50   Input ~ 0
108
Text GLabel 1250 3350 1    50   Input ~ 0
107
Text GLabel 1350 3350 1    50   Input ~ 0
106
Text GLabel 1450 3350 1    50   Input ~ 0
105
Text GLabel 1550 3350 1    50   Input ~ 0
104
Text GLabel 1650 3350 1    50   Input ~ 0
103
Text GLabel 1750 3350 1    50   Input ~ 0
102
Text GLabel 1850 3350 1    50   Input ~ 0
101
Text GLabel 1950 3350 1    50   Input ~ 0
100
Text GLabel 2050 3350 1    50   Input ~ 0
99
Text GLabel 2150 3350 1    50   Input ~ 0
98
Text GLabel 2250 3350 1    50   Input ~ 0
97
Text GLabel 5950 1400 0    50   Input ~ 0
1
Text GLabel 5950 1500 0    50   Input ~ 0
2
Text GLabel 5950 1600 0    50   Input ~ 0
3
Text GLabel 5950 1700 0    50   Input ~ 0
4
Text GLabel 5950 1800 0    50   Input ~ 0
5
Text GLabel 5950 1900 0    50   Input ~ 0
6
Text GLabel 5950 2000 0    50   Input ~ 0
7
Text GLabel 5950 2100 0    50   Input ~ 0
8
Text GLabel 5950 2200 0    50   Input ~ 0
9
Text GLabel 5950 2300 0    50   Input ~ 0
10
Text GLabel 5950 2400 0    50   Input ~ 0
11
Text GLabel 5950 2500 0    50   Input ~ 0
12
Text GLabel 5950 2600 0    50   Input ~ 0
13
Text GLabel 5950 2700 0    50   Input ~ 0
14
Text GLabel 5950 2800 0    50   Input ~ 0
15
Text GLabel 5950 2900 0    50   Input ~ 0
16
Text GLabel 5950 3000 0    50   Input ~ 0
17
Text GLabel 5950 3100 0    50   Input ~ 0
18
Text GLabel 5950 3200 0    50   Input ~ 0
19
Text GLabel 5950 3300 0    50   Input ~ 0
20
Text GLabel 5950 3400 0    50   Input ~ 0
21
Text GLabel 5950 3500 0    50   Input ~ 0
22
Text GLabel 5950 3600 0    50   Input ~ 0
23
Text GLabel 5950 3700 0    50   Input ~ 0
24
Text GLabel 5950 3800 0    50   Input ~ 0
25
Text GLabel 5950 3900 0    50   Input ~ 0
26
Text GLabel 5950 4000 0    50   Input ~ 0
27
Text GLabel 5950 4100 0    50   Input ~ 0
28
Text GLabel 5950 4200 0    50   Input ~ 0
29
Text GLabel 5950 4300 0    50   Input ~ 0
30
Text GLabel 5950 4400 0    50   Input ~ 0
31
Text GLabel 5950 4500 0    50   Input ~ 0
32
Text GLabel 9850 5000 3    50   Input ~ 0
64
Text GLabel 9750 5000 3    50   Input ~ 0
63
Text GLabel 9650 5000 3    50   Input ~ 0
62
Text GLabel 9550 5000 3    50   Input ~ 0
61
Text GLabel 9450 5000 3    50   Input ~ 0
60
Text GLabel 9350 5000 3    50   Input ~ 0
59
Text GLabel 9250 5000 3    50   Input ~ 0
58
Text GLabel 9150 5000 3    50   Input ~ 0
57
Text GLabel 9050 5000 3    50   Input ~ 0
56
Text GLabel 8950 5000 3    50   Input ~ 0
55
Text GLabel 8850 5000 3    50   Input ~ 0
54
Text GLabel 8750 5000 3    50   Input ~ 0
53
Text GLabel 8650 5000 3    50   Input ~ 0
52
Text GLabel 8550 5000 3    50   Input ~ 0
51
Text GLabel 8450 5000 3    50   Input ~ 0
50
Text GLabel 8350 5000 3    50   Input ~ 0
49
Text GLabel 8250 5000 3    50   Input ~ 0
48
Text GLabel 8150 5000 3    50   Input ~ 0
47
Text GLabel 8050 5000 3    50   Input ~ 0
46
Text GLabel 7950 5000 3    50   Input ~ 0
45
Text GLabel 7850 5000 3    50   Input ~ 0
44
Text GLabel 7750 5000 3    50   Input ~ 0
43
Text GLabel 7650 5000 3    50   Input ~ 0
42
Text GLabel 7550 5000 3    50   Input ~ 0
41
Text GLabel 7450 5000 3    50   Input ~ 0
40
Text GLabel 7350 5000 3    50   Input ~ 0
39
Text GLabel 7250 5000 3    50   Input ~ 0
38
Text GLabel 7150 5000 3    50   Input ~ 0
37
Text GLabel 7050 5000 3    50   Input ~ 0
36
Text GLabel 6950 5000 3    50   Input ~ 0
35
Text GLabel 6850 5000 3    50   Input ~ 0
34
Text GLabel 6750 5000 3    50   Input ~ 0
33
Text GLabel 10650 4500 2    50   Input ~ 0
65
Text GLabel 10650 4400 2    50   Input ~ 0
66
Text GLabel 10650 4300 2    50   Input ~ 0
67
Text GLabel 10650 4200 2    50   Input ~ 0
68
Text GLabel 10650 4100 2    50   Input ~ 0
69
Text GLabel 10650 4000 2    50   Input ~ 0
70
Text GLabel 10650 3900 2    50   Input ~ 0
71
Text GLabel 10650 3800 2    50   Input ~ 0
72
Text GLabel 10650 3700 2    50   Input ~ 0
73
Text GLabel 10650 3600 2    50   Input ~ 0
74
Text GLabel 10650 3500 2    50   Input ~ 0
75
Text GLabel 10650 3400 2    50   Input ~ 0
76
Text GLabel 10650 3300 2    50   Input ~ 0
77
Text GLabel 10650 3200 2    50   Input ~ 0
78
Text GLabel 10650 3100 2    50   Input ~ 0
79
Text GLabel 10650 3000 2    50   Input ~ 0
80
Text GLabel 10650 2900 2    50   Input ~ 0
81
Text GLabel 10650 2800 2    50   Input ~ 0
82
Text GLabel 10650 2700 2    50   Input ~ 0
83
Text GLabel 10650 2600 2    50   Input ~ 0
84
Text GLabel 10650 2500 2    50   Input ~ 0
85
Text GLabel 10650 2400 2    50   Input ~ 0
86
Text GLabel 10650 2300 2    50   Input ~ 0
87
Text GLabel 10650 2200 2    50   Input ~ 0
88
Text GLabel 10650 2100 2    50   Input ~ 0
89
Text GLabel 10650 2000 2    50   Input ~ 0
90
Text GLabel 10650 1900 2    50   Input ~ 0
91
Text GLabel 10650 1800 2    50   Input ~ 0
92
Text GLabel 10650 1700 2    50   Input ~ 0
93
Text GLabel 10650 1600 2    50   Input ~ 0
94
Text GLabel 10650 1500 2    50   Input ~ 0
95
Text GLabel 10650 1400 2    50   Input ~ 0
96
Text GLabel 6750 900  1    50   Input ~ 0
128
Text GLabel 6850 900  1    50   Input ~ 0
127
Text GLabel 6950 900  1    50   Input ~ 0
126
Text GLabel 7050 900  1    50   Input ~ 0
125
Text GLabel 7150 900  1    50   Input ~ 0
124
Text GLabel 7250 900  1    50   Input ~ 0
123
Text GLabel 7350 900  1    50   Input ~ 0
122
Text GLabel 7450 900  1    50   Input ~ 0
121
Text GLabel 7550 900  1    50   Input ~ 0
120
Text GLabel 7650 900  1    50   Input ~ 0
119
Text GLabel 7750 900  1    50   Input ~ 0
118
Text GLabel 7850 900  1    50   Input ~ 0
117
Text GLabel 7950 900  1    50   Input ~ 0
116
Text GLabel 8050 900  1    50   Input ~ 0
115
Text GLabel 8150 900  1    50   Input ~ 0
114
Text GLabel 8250 900  1    50   Input ~ 0
113
Text GLabel 8350 900  1    50   Input ~ 0
112
Text GLabel 8450 900  1    50   Input ~ 0
111
Text GLabel 8550 900  1    50   Input ~ 0
110
Text GLabel 8650 900  1    50   Input ~ 0
109
Text GLabel 8750 900  1    50   Input ~ 0
108
Text GLabel 8850 900  1    50   Input ~ 0
107
Text GLabel 8950 900  1    50   Input ~ 0
106
Text GLabel 9050 900  1    50   Input ~ 0
105
Text GLabel 9150 900  1    50   Input ~ 0
104
Text GLabel 9250 900  1    50   Input ~ 0
103
Text GLabel 9350 900  1    50   Input ~ 0
102
Text GLabel 9450 900  1    50   Input ~ 0
101
Text GLabel 9550 900  1    50   Input ~ 0
100
Text GLabel 9650 900  1    50   Input ~ 0
99
Text GLabel 9750 900  1    50   Input ~ 0
98
Text GLabel 9850 900  1    50   Input ~ 0
97
$Comp
L Device:C C1
U 1 1 5CD47EE1
P -6000 3350
F 0 "C1" V -6100 3150 50  0000 C CNN
F 1 "C" V -6100 3500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5962 3200 50  0001 C CNN
F 3 "~" H -6000 3350 50  0001 C CNN
	1    -6000 3350
	0    1    1    0   
$EndComp
$Comp
L Device:C C2
U 1 1 5CD7F44C
P -6000 3550
F 0 "C2" V -6100 3350 50  0000 C CNN
F 1 "C" V -6100 3700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5962 3400 50  0001 C CNN
F 3 "~" H -6000 3550 50  0001 C CNN
	1    -6000 3550
	0    1    1    0   
$EndComp
$Comp
L Device:C C3
U 1 1 5CD81290
P -6000 3750
F 0 "C3" V -6100 3550 50  0000 C CNN
F 1 "C" V -6100 3900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5962 3600 50  0001 C CNN
F 3 "~" H -6000 3750 50  0001 C CNN
	1    -6000 3750
	0    1    1    0   
$EndComp
$Comp
L Device:C C4
U 1 1 5CD81296
P -6000 3950
F 0 "C4" V -6100 3750 50  0000 C CNN
F 1 "C" V -6100 4100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5962 3800 50  0001 C CNN
F 3 "~" H -6000 3950 50  0001 C CNN
	1    -6000 3950
	0    1    1    0   
$EndComp
$Comp
L Device:C C5
U 1 1 5CD8251C
P -6000 4150
F 0 "C5" V -6100 3950 50  0000 C CNN
F 1 "C" V -6100 4300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5962 4000 50  0001 C CNN
F 3 "~" H -6000 4150 50  0001 C CNN
	1    -6000 4150
	0    1    1    0   
$EndComp
$Comp
L Device:C C6
U 1 1 5CD82522
P -6000 4350
F 0 "C6" V -6100 4150 50  0000 C CNN
F 1 "C" V -6100 4500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5962 4200 50  0001 C CNN
F 3 "~" H -6000 4350 50  0001 C CNN
	1    -6000 4350
	0    1    1    0   
$EndComp
$Comp
L Device:C C7
U 1 1 5CD82528
P -6000 4550
F 0 "C7" V -6100 4350 50  0000 C CNN
F 1 "C" V -6100 4700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5962 4400 50  0001 C CNN
F 3 "~" H -6000 4550 50  0001 C CNN
	1    -6000 4550
	0    1    1    0   
$EndComp
$Comp
L Device:C C8
U 1 1 5CD8252E
P -6000 4750
F 0 "C8" V -6100 4550 50  0000 C CNN
F 1 "C" V -6100 4900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5962 4600 50  0001 C CNN
F 3 "~" H -6000 4750 50  0001 C CNN
	1    -6000 4750
	0    1    1    0   
$EndComp
$Comp
L Device:C C9
U 1 1 5CD84684
P -6000 4950
F 0 "C9" V -6100 4750 50  0000 C CNN
F 1 "C" V -6100 5100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5962 4800 50  0001 C CNN
F 3 "~" H -6000 4950 50  0001 C CNN
	1    -6000 4950
	0    1    1    0   
$EndComp
$Comp
L Device:C C10
U 1 1 5CD8468A
P -6000 5150
F 0 "C10" V -6100 4950 50  0000 C CNN
F 1 "C" V -6100 5300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5962 5000 50  0001 C CNN
F 3 "~" H -6000 5150 50  0001 C CNN
	1    -6000 5150
	0    1    1    0   
$EndComp
$Comp
L Device:C C11
U 1 1 5CD84690
P -6000 5350
F 0 "C11" V -6100 5150 50  0000 C CNN
F 1 "C" V -6100 5500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5962 5200 50  0001 C CNN
F 3 "~" H -6000 5350 50  0001 C CNN
	1    -6000 5350
	0    1    1    0   
$EndComp
$Comp
L Device:C C12
U 1 1 5CD84696
P -6000 5550
F 0 "C12" V -6100 5350 50  0000 C CNN
F 1 "C" V -6100 5700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5962 5400 50  0001 C CNN
F 3 "~" H -6000 5550 50  0001 C CNN
	1    -6000 5550
	0    1    1    0   
$EndComp
$Comp
L Device:C C13
U 1 1 5CD8469C
P -6000 5750
F 0 "C13" V -6100 5550 50  0000 C CNN
F 1 "C" V -6100 5900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5962 5600 50  0001 C CNN
F 3 "~" H -6000 5750 50  0001 C CNN
	1    -6000 5750
	0    1    1    0   
$EndComp
$Comp
L Device:C C14
U 1 1 5CD846A2
P -6000 5950
F 0 "C14" V -6100 5750 50  0000 C CNN
F 1 "C" V -6100 6100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5962 5800 50  0001 C CNN
F 3 "~" H -6000 5950 50  0001 C CNN
	1    -6000 5950
	0    1    1    0   
$EndComp
$Comp
L Device:C C15
U 1 1 5CD846A8
P -6000 6150
F 0 "C15" V -6100 5950 50  0000 C CNN
F 1 "C" V -6100 6300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5962 6000 50  0001 C CNN
F 3 "~" H -6000 6150 50  0001 C CNN
	1    -6000 6150
	0    1    1    0   
$EndComp
$Comp
L Device:C C16
U 1 1 5CD846AE
P -6000 6350
F 0 "C16" V -6100 6150 50  0000 C CNN
F 1 "C" V -6100 6500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5962 6200 50  0001 C CNN
F 3 "~" H -6000 6350 50  0001 C CNN
	1    -6000 6350
	0    1    1    0   
$EndComp
$Comp
L Device:C C17
U 1 1 5CD9E568
P -6000 6550
F 0 "C17" V -6100 6350 50  0000 C CNN
F 1 "C" V -6100 6700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5962 6400 50  0001 C CNN
F 3 "~" H -6000 6550 50  0001 C CNN
	1    -6000 6550
	0    1    1    0   
$EndComp
$Comp
L Device:C C18
U 1 1 5CD9E56E
P -6000 6750
F 0 "C18" V -6100 6550 50  0000 C CNN
F 1 "C" V -6100 6900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5962 6600 50  0001 C CNN
F 3 "~" H -6000 6750 50  0001 C CNN
	1    -6000 6750
	0    1    1    0   
$EndComp
$Comp
L Device:C C19
U 1 1 5CD9E574
P -6000 6950
F 0 "C19" V -6100 6750 50  0000 C CNN
F 1 "C" V -6100 7100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5962 6800 50  0001 C CNN
F 3 "~" H -6000 6950 50  0001 C CNN
	1    -6000 6950
	0    1    1    0   
$EndComp
$Comp
L Device:C C20
U 1 1 5CD9E57A
P -6000 7150
F 0 "C20" V -6100 6950 50  0000 C CNN
F 1 "C" V -6100 7300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5962 7000 50  0001 C CNN
F 3 "~" H -6000 7150 50  0001 C CNN
	1    -6000 7150
	0    1    1    0   
$EndComp
$Comp
L Device:C C21
U 1 1 5CD9E580
P -6000 7350
F 0 "C21" V -6100 7150 50  0000 C CNN
F 1 "C" V -6100 7500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5962 7200 50  0001 C CNN
F 3 "~" H -6000 7350 50  0001 C CNN
	1    -6000 7350
	0    1    1    0   
$EndComp
$Comp
L Device:C C22
U 1 1 5CD9E586
P -6000 7550
F 0 "C22" V -6100 7350 50  0000 C CNN
F 1 "C" V -6100 7700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5962 7400 50  0001 C CNN
F 3 "~" H -6000 7550 50  0001 C CNN
	1    -6000 7550
	0    1    1    0   
$EndComp
$Comp
L Device:C C23
U 1 1 5CD9E58C
P -6000 7750
F 0 "C23" V -6100 7550 50  0000 C CNN
F 1 "C" V -6100 7900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5962 7600 50  0001 C CNN
F 3 "~" H -6000 7750 50  0001 C CNN
	1    -6000 7750
	0    1    1    0   
$EndComp
$Comp
L Device:C C24
U 1 1 5CD9E592
P -6000 7950
F 0 "C24" V -6100 7750 50  0000 C CNN
F 1 "C" V -6100 8100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5962 7800 50  0001 C CNN
F 3 "~" H -6000 7950 50  0001 C CNN
	1    -6000 7950
	0    1    1    0   
$EndComp
$Comp
L Device:C C25
U 1 1 5CD9E598
P -6000 8150
F 0 "C25" V -6100 7950 50  0000 C CNN
F 1 "C" V -6100 8300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5962 8000 50  0001 C CNN
F 3 "~" H -6000 8150 50  0001 C CNN
	1    -6000 8150
	0    1    1    0   
$EndComp
$Comp
L Device:C C26
U 1 1 5CD9E59E
P -6000 8350
F 0 "C26" V -6100 8150 50  0000 C CNN
F 1 "C" V -6100 8500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5962 8200 50  0001 C CNN
F 3 "~" H -6000 8350 50  0001 C CNN
	1    -6000 8350
	0    1    1    0   
$EndComp
$Comp
L Device:C C27
U 1 1 5CD9E5A4
P -6000 8550
F 0 "C27" V -6100 8350 50  0000 C CNN
F 1 "C" V -6100 8700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5962 8400 50  0001 C CNN
F 3 "~" H -6000 8550 50  0001 C CNN
	1    -6000 8550
	0    1    1    0   
$EndComp
$Comp
L Device:C C28
U 1 1 5CD9E5AA
P -6000 8750
F 0 "C28" V -6100 8550 50  0000 C CNN
F 1 "C" V -6100 8900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5962 8600 50  0001 C CNN
F 3 "~" H -6000 8750 50  0001 C CNN
	1    -6000 8750
	0    1    1    0   
$EndComp
$Comp
L Device:C C29
U 1 1 5CD9E5B0
P -6000 8950
F 0 "C29" V -6100 8750 50  0000 C CNN
F 1 "C" V -6100 9100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5962 8800 50  0001 C CNN
F 3 "~" H -6000 8950 50  0001 C CNN
	1    -6000 8950
	0    1    1    0   
$EndComp
$Comp
L Device:C C30
U 1 1 5CD9E5B6
P -6000 9150
F 0 "C30" V -6100 8950 50  0000 C CNN
F 1 "C" V -6100 9300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5962 9000 50  0001 C CNN
F 3 "~" H -6000 9150 50  0001 C CNN
	1    -6000 9150
	0    1    1    0   
$EndComp
$Comp
L Device:C C31
U 1 1 5CD9E5BC
P -6000 9350
F 0 "C31" V -6100 9150 50  0000 C CNN
F 1 "C" V -6100 9500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5962 9200 50  0001 C CNN
F 3 "~" H -6000 9350 50  0001 C CNN
	1    -6000 9350
	0    1    1    0   
$EndComp
$Comp
L Device:C C32
U 1 1 5CD9E5C2
P -6000 9550
F 0 "C32" V -6100 9350 50  0000 C CNN
F 1 "C" V -6100 9700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5962 9400 50  0001 C CNN
F 3 "~" H -6000 9550 50  0001 C CNN
	1    -6000 9550
	0    1    1    0   
$EndComp
$Comp
L Device:C C33
U 1 1 5CDB9360
P -5300 3350
F 0 "C33" V -5400 3150 50  0000 C CNN
F 1 "C" V -5400 3500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5262 3200 50  0001 C CNN
F 3 "~" H -5300 3350 50  0001 C CNN
	1    -5300 3350
	0    1    1    0   
$EndComp
$Comp
L Device:C C34
U 1 1 5CDB9366
P -5300 3550
F 0 "C34" V -5400 3350 50  0000 C CNN
F 1 "C" V -5400 3700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5262 3400 50  0001 C CNN
F 3 "~" H -5300 3550 50  0001 C CNN
	1    -5300 3550
	0    1    1    0   
$EndComp
$Comp
L Device:C C35
U 1 1 5CDB936C
P -5300 3750
F 0 "C35" V -5400 3550 50  0000 C CNN
F 1 "C" V -5400 3900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5262 3600 50  0001 C CNN
F 3 "~" H -5300 3750 50  0001 C CNN
	1    -5300 3750
	0    1    1    0   
$EndComp
$Comp
L Device:C C36
U 1 1 5CDB9372
P -5300 3950
F 0 "C36" V -5400 3750 50  0000 C CNN
F 1 "C" V -5400 4100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5262 3800 50  0001 C CNN
F 3 "~" H -5300 3950 50  0001 C CNN
	1    -5300 3950
	0    1    1    0   
$EndComp
$Comp
L Device:C C37
U 1 1 5CDB9378
P -5300 4150
F 0 "C37" V -5400 3950 50  0000 C CNN
F 1 "C" V -5400 4300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5262 4000 50  0001 C CNN
F 3 "~" H -5300 4150 50  0001 C CNN
	1    -5300 4150
	0    1    1    0   
$EndComp
$Comp
L Device:C C38
U 1 1 5CDB937E
P -5300 4350
F 0 "C38" V -5400 4150 50  0000 C CNN
F 1 "C" V -5400 4500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5262 4200 50  0001 C CNN
F 3 "~" H -5300 4350 50  0001 C CNN
	1    -5300 4350
	0    1    1    0   
$EndComp
$Comp
L Device:C C39
U 1 1 5CDB9384
P -5300 4550
F 0 "C39" V -5400 4350 50  0000 C CNN
F 1 "C" V -5400 4700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5262 4400 50  0001 C CNN
F 3 "~" H -5300 4550 50  0001 C CNN
	1    -5300 4550
	0    1    1    0   
$EndComp
$Comp
L Device:C C40
U 1 1 5CDB938A
P -5300 4750
F 0 "C40" V -5400 4550 50  0000 C CNN
F 1 "C" V -5400 4900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5262 4600 50  0001 C CNN
F 3 "~" H -5300 4750 50  0001 C CNN
	1    -5300 4750
	0    1    1    0   
$EndComp
$Comp
L Device:C C41
U 1 1 5CDB9390
P -5300 4950
F 0 "C41" V -5400 4750 50  0000 C CNN
F 1 "C" V -5400 5100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5262 4800 50  0001 C CNN
F 3 "~" H -5300 4950 50  0001 C CNN
	1    -5300 4950
	0    1    1    0   
$EndComp
$Comp
L Device:C C42
U 1 1 5CDB9396
P -5300 5150
F 0 "C42" V -5400 4950 50  0000 C CNN
F 1 "C" V -5400 5300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5262 5000 50  0001 C CNN
F 3 "~" H -5300 5150 50  0001 C CNN
	1    -5300 5150
	0    1    1    0   
$EndComp
$Comp
L Device:C C43
U 1 1 5CDB939C
P -5300 5350
F 0 "C43" V -5400 5150 50  0000 C CNN
F 1 "C" V -5400 5500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5262 5200 50  0001 C CNN
F 3 "~" H -5300 5350 50  0001 C CNN
	1    -5300 5350
	0    1    1    0   
$EndComp
$Comp
L Device:C C44
U 1 1 5CDB93A2
P -5300 5550
F 0 "C44" V -5400 5350 50  0000 C CNN
F 1 "C" V -5400 5700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5262 5400 50  0001 C CNN
F 3 "~" H -5300 5550 50  0001 C CNN
	1    -5300 5550
	0    1    1    0   
$EndComp
$Comp
L Device:C C45
U 1 1 5CDB93A8
P -5300 5750
F 0 "C45" V -5400 5550 50  0000 C CNN
F 1 "C" V -5400 5900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5262 5600 50  0001 C CNN
F 3 "~" H -5300 5750 50  0001 C CNN
	1    -5300 5750
	0    1    1    0   
$EndComp
$Comp
L Device:C C46
U 1 1 5CDB93AE
P -5300 5950
F 0 "C46" V -5400 5750 50  0000 C CNN
F 1 "C" V -5400 6100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5262 5800 50  0001 C CNN
F 3 "~" H -5300 5950 50  0001 C CNN
	1    -5300 5950
	0    1    1    0   
$EndComp
$Comp
L Device:C C47
U 1 1 5CDB93B4
P -5300 6150
F 0 "C47" V -5400 5950 50  0000 C CNN
F 1 "C" V -5400 6300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5262 6000 50  0001 C CNN
F 3 "~" H -5300 6150 50  0001 C CNN
	1    -5300 6150
	0    1    1    0   
$EndComp
$Comp
L Device:C C48
U 1 1 5CDB93BA
P -5300 6350
F 0 "C48" V -5400 6150 50  0000 C CNN
F 1 "C" V -5400 6500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5262 6200 50  0001 C CNN
F 3 "~" H -5300 6350 50  0001 C CNN
	1    -5300 6350
	0    1    1    0   
$EndComp
$Comp
L Device:C C49
U 1 1 5CDB93C0
P -5300 6550
F 0 "C49" V -5400 6350 50  0000 C CNN
F 1 "C" V -5400 6700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5262 6400 50  0001 C CNN
F 3 "~" H -5300 6550 50  0001 C CNN
	1    -5300 6550
	0    1    1    0   
$EndComp
$Comp
L Device:C C50
U 1 1 5CDB93C6
P -5300 6750
F 0 "C50" V -5400 6550 50  0000 C CNN
F 1 "C" V -5400 6900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5262 6600 50  0001 C CNN
F 3 "~" H -5300 6750 50  0001 C CNN
	1    -5300 6750
	0    1    1    0   
$EndComp
$Comp
L Device:C C51
U 1 1 5CDB93CC
P -5300 6950
F 0 "C51" V -5400 6750 50  0000 C CNN
F 1 "C" V -5400 7100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5262 6800 50  0001 C CNN
F 3 "~" H -5300 6950 50  0001 C CNN
	1    -5300 6950
	0    1    1    0   
$EndComp
$Comp
L Device:C C52
U 1 1 5CDB93D2
P -5300 7150
F 0 "C52" V -5400 6950 50  0000 C CNN
F 1 "C" V -5400 7300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5262 7000 50  0001 C CNN
F 3 "~" H -5300 7150 50  0001 C CNN
	1    -5300 7150
	0    1    1    0   
$EndComp
$Comp
L Device:C C53
U 1 1 5CDB93D8
P -5300 7350
F 0 "C53" V -5400 7150 50  0000 C CNN
F 1 "C" V -5400 7500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5262 7200 50  0001 C CNN
F 3 "~" H -5300 7350 50  0001 C CNN
	1    -5300 7350
	0    1    1    0   
$EndComp
$Comp
L Device:C C54
U 1 1 5CDB93DE
P -5300 7550
F 0 "C54" V -5400 7350 50  0000 C CNN
F 1 "C" V -5400 7700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5262 7400 50  0001 C CNN
F 3 "~" H -5300 7550 50  0001 C CNN
	1    -5300 7550
	0    1    1    0   
$EndComp
$Comp
L Device:C C55
U 1 1 5CDB93E4
P -5300 7750
F 0 "C55" V -5400 7550 50  0000 C CNN
F 1 "C" V -5400 7900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5262 7600 50  0001 C CNN
F 3 "~" H -5300 7750 50  0001 C CNN
	1    -5300 7750
	0    1    1    0   
$EndComp
$Comp
L Device:C C56
U 1 1 5CDB93EA
P -5300 7950
F 0 "C56" V -5400 7750 50  0000 C CNN
F 1 "C" V -5400 8100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5262 7800 50  0001 C CNN
F 3 "~" H -5300 7950 50  0001 C CNN
	1    -5300 7950
	0    1    1    0   
$EndComp
$Comp
L Device:C C57
U 1 1 5CDB93F0
P -5300 8150
F 0 "C57" V -5400 7950 50  0000 C CNN
F 1 "C" V -5400 8300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5262 8000 50  0001 C CNN
F 3 "~" H -5300 8150 50  0001 C CNN
	1    -5300 8150
	0    1    1    0   
$EndComp
$Comp
L Device:C C58
U 1 1 5CDB93F6
P -5300 8350
F 0 "C58" V -5400 8150 50  0000 C CNN
F 1 "C" V -5400 8500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5262 8200 50  0001 C CNN
F 3 "~" H -5300 8350 50  0001 C CNN
	1    -5300 8350
	0    1    1    0   
$EndComp
$Comp
L Device:C C59
U 1 1 5CDB93FC
P -5300 8550
F 0 "C59" V -5400 8350 50  0000 C CNN
F 1 "C" V -5400 8700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5262 8400 50  0001 C CNN
F 3 "~" H -5300 8550 50  0001 C CNN
	1    -5300 8550
	0    1    1    0   
$EndComp
$Comp
L Device:C C60
U 1 1 5CDB9402
P -5300 8750
F 0 "C60" V -5400 8550 50  0000 C CNN
F 1 "C" V -5400 8900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5262 8600 50  0001 C CNN
F 3 "~" H -5300 8750 50  0001 C CNN
	1    -5300 8750
	0    1    1    0   
$EndComp
$Comp
L Device:C C61
U 1 1 5CDB9408
P -5300 8950
F 0 "C61" V -5400 8750 50  0000 C CNN
F 1 "C" V -5400 9100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5262 8800 50  0001 C CNN
F 3 "~" H -5300 8950 50  0001 C CNN
	1    -5300 8950
	0    1    1    0   
$EndComp
$Comp
L Device:C C62
U 1 1 5CDB940E
P -5300 9150
F 0 "C62" V -5400 8950 50  0000 C CNN
F 1 "C" V -5400 9300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5262 9000 50  0001 C CNN
F 3 "~" H -5300 9150 50  0001 C CNN
	1    -5300 9150
	0    1    1    0   
$EndComp
$Comp
L Device:C C63
U 1 1 5CDB9414
P -5300 9350
F 0 "C63" V -5400 9150 50  0000 C CNN
F 1 "C" V -5400 9500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5262 9200 50  0001 C CNN
F 3 "~" H -5300 9350 50  0001 C CNN
	1    -5300 9350
	0    1    1    0   
$EndComp
$Comp
L Device:C C64
U 1 1 5CDB941A
P -5300 9550
F 0 "C64" V -5400 9350 50  0000 C CNN
F 1 "C" V -5400 9700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -5262 9400 50  0001 C CNN
F 3 "~" H -5300 9550 50  0001 C CNN
	1    -5300 9550
	0    1    1    0   
$EndComp
$Comp
L Device:C C65
U 1 1 5CDF2A0E
P -4550 3350
F 0 "C65" V -4650 3150 50  0000 C CNN
F 1 "C" V -4650 3500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -4512 3200 50  0001 C CNN
F 3 "~" H -4550 3350 50  0001 C CNN
	1    -4550 3350
	0    1    1    0   
$EndComp
$Comp
L Device:C C66
U 1 1 5CDF2A14
P -4550 3550
F 0 "C66" V -4650 3350 50  0000 C CNN
F 1 "C" V -4650 3700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -4512 3400 50  0001 C CNN
F 3 "~" H -4550 3550 50  0001 C CNN
	1    -4550 3550
	0    1    1    0   
$EndComp
$Comp
L Device:C C67
U 1 1 5CDF2A1A
P -4550 3750
F 0 "C67" V -4650 3550 50  0000 C CNN
F 1 "C" V -4650 3900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -4512 3600 50  0001 C CNN
F 3 "~" H -4550 3750 50  0001 C CNN
	1    -4550 3750
	0    1    1    0   
$EndComp
$Comp
L Device:C C68
U 1 1 5CDF2A20
P -4550 3950
F 0 "C68" V -4650 3750 50  0000 C CNN
F 1 "C" V -4650 4100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -4512 3800 50  0001 C CNN
F 3 "~" H -4550 3950 50  0001 C CNN
	1    -4550 3950
	0    1    1    0   
$EndComp
$Comp
L Device:C C69
U 1 1 5CDF2A26
P -4550 4150
F 0 "C69" V -4650 3950 50  0000 C CNN
F 1 "C" V -4650 4300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -4512 4000 50  0001 C CNN
F 3 "~" H -4550 4150 50  0001 C CNN
	1    -4550 4150
	0    1    1    0   
$EndComp
$Comp
L Device:C C70
U 1 1 5CDF2A2C
P -4550 4350
F 0 "C70" V -4650 4150 50  0000 C CNN
F 1 "C" V -4650 4500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -4512 4200 50  0001 C CNN
F 3 "~" H -4550 4350 50  0001 C CNN
	1    -4550 4350
	0    1    1    0   
$EndComp
$Comp
L Device:C C71
U 1 1 5CDF2A32
P -4550 4550
F 0 "C71" V -4650 4350 50  0000 C CNN
F 1 "C" V -4650 4700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -4512 4400 50  0001 C CNN
F 3 "~" H -4550 4550 50  0001 C CNN
	1    -4550 4550
	0    1    1    0   
$EndComp
$Comp
L Device:C C72
U 1 1 5CDF2A38
P -4550 4750
F 0 "C72" V -4650 4550 50  0000 C CNN
F 1 "C" V -4650 4900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -4512 4600 50  0001 C CNN
F 3 "~" H -4550 4750 50  0001 C CNN
	1    -4550 4750
	0    1    1    0   
$EndComp
$Comp
L Device:C C73
U 1 1 5CDF2A3E
P -4550 4950
F 0 "C73" V -4650 4750 50  0000 C CNN
F 1 "C" V -4650 5100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -4512 4800 50  0001 C CNN
F 3 "~" H -4550 4950 50  0001 C CNN
	1    -4550 4950
	0    1    1    0   
$EndComp
$Comp
L Device:C C74
U 1 1 5CDF2A44
P -4550 5150
F 0 "C74" V -4650 4950 50  0000 C CNN
F 1 "C" V -4650 5300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -4512 5000 50  0001 C CNN
F 3 "~" H -4550 5150 50  0001 C CNN
	1    -4550 5150
	0    1    1    0   
$EndComp
$Comp
L Device:C C75
U 1 1 5CDF2A4A
P -4550 5350
F 0 "C75" V -4650 5150 50  0000 C CNN
F 1 "C" V -4650 5500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -4512 5200 50  0001 C CNN
F 3 "~" H -4550 5350 50  0001 C CNN
	1    -4550 5350
	0    1    1    0   
$EndComp
$Comp
L Device:C C76
U 1 1 5CDF2A50
P -4550 5550
F 0 "C76" V -4650 5350 50  0000 C CNN
F 1 "C" V -4650 5700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -4512 5400 50  0001 C CNN
F 3 "~" H -4550 5550 50  0001 C CNN
	1    -4550 5550
	0    1    1    0   
$EndComp
$Comp
L Device:C C77
U 1 1 5CDF2A56
P -4550 5750
F 0 "C77" V -4650 5550 50  0000 C CNN
F 1 "C" V -4650 5900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -4512 5600 50  0001 C CNN
F 3 "~" H -4550 5750 50  0001 C CNN
	1    -4550 5750
	0    1    1    0   
$EndComp
$Comp
L Device:C C78
U 1 1 5CDF2A5C
P -4550 5950
F 0 "C78" V -4650 5750 50  0000 C CNN
F 1 "C" V -4650 6100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -4512 5800 50  0001 C CNN
F 3 "~" H -4550 5950 50  0001 C CNN
	1    -4550 5950
	0    1    1    0   
$EndComp
$Comp
L Device:C C79
U 1 1 5CDF2A62
P -4550 6150
F 0 "C79" V -4650 5950 50  0000 C CNN
F 1 "C" V -4650 6300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -4512 6000 50  0001 C CNN
F 3 "~" H -4550 6150 50  0001 C CNN
	1    -4550 6150
	0    1    1    0   
$EndComp
$Comp
L Device:C C80
U 1 1 5CDF2A68
P -4550 6350
F 0 "C80" V -4650 6150 50  0000 C CNN
F 1 "C" V -4650 6500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -4512 6200 50  0001 C CNN
F 3 "~" H -4550 6350 50  0001 C CNN
	1    -4550 6350
	0    1    1    0   
$EndComp
$Comp
L Device:C C81
U 1 1 5CDF2A6E
P -4550 6550
F 0 "C81" V -4650 6350 50  0000 C CNN
F 1 "C" V -4650 6700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -4512 6400 50  0001 C CNN
F 3 "~" H -4550 6550 50  0001 C CNN
	1    -4550 6550
	0    1    1    0   
$EndComp
$Comp
L Device:C C82
U 1 1 5CDF2A74
P -4550 6750
F 0 "C82" V -4650 6550 50  0000 C CNN
F 1 "C" V -4650 6900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -4512 6600 50  0001 C CNN
F 3 "~" H -4550 6750 50  0001 C CNN
	1    -4550 6750
	0    1    1    0   
$EndComp
$Comp
L Device:C C83
U 1 1 5CDF2A7A
P -4550 6950
F 0 "C83" V -4650 6750 50  0000 C CNN
F 1 "C" V -4650 7100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -4512 6800 50  0001 C CNN
F 3 "~" H -4550 6950 50  0001 C CNN
	1    -4550 6950
	0    1    1    0   
$EndComp
$Comp
L Device:C C84
U 1 1 5CDF2A80
P -4550 7150
F 0 "C84" V -4650 6950 50  0000 C CNN
F 1 "C" V -4650 7300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -4512 7000 50  0001 C CNN
F 3 "~" H -4550 7150 50  0001 C CNN
	1    -4550 7150
	0    1    1    0   
$EndComp
$Comp
L Device:C C85
U 1 1 5CDF2A86
P -4550 7350
F 0 "C85" V -4650 7150 50  0000 C CNN
F 1 "C" V -4650 7500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -4512 7200 50  0001 C CNN
F 3 "~" H -4550 7350 50  0001 C CNN
	1    -4550 7350
	0    1    1    0   
$EndComp
$Comp
L Device:C C86
U 1 1 5CDF2A8C
P -4550 7550
F 0 "C86" V -4650 7350 50  0000 C CNN
F 1 "C" V -4650 7700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -4512 7400 50  0001 C CNN
F 3 "~" H -4550 7550 50  0001 C CNN
	1    -4550 7550
	0    1    1    0   
$EndComp
$Comp
L Device:C C87
U 1 1 5CDF2A92
P -4550 7750
F 0 "C87" V -4650 7550 50  0000 C CNN
F 1 "C" V -4650 7900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -4512 7600 50  0001 C CNN
F 3 "~" H -4550 7750 50  0001 C CNN
	1    -4550 7750
	0    1    1    0   
$EndComp
$Comp
L Device:C C88
U 1 1 5CDF2A98
P -4550 7950
F 0 "C88" V -4650 7750 50  0000 C CNN
F 1 "C" V -4650 8100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -4512 7800 50  0001 C CNN
F 3 "~" H -4550 7950 50  0001 C CNN
	1    -4550 7950
	0    1    1    0   
$EndComp
$Comp
L Device:C C89
U 1 1 5CDF2A9E
P -4550 8150
F 0 "C89" V -4650 7950 50  0000 C CNN
F 1 "C" V -4650 8300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -4512 8000 50  0001 C CNN
F 3 "~" H -4550 8150 50  0001 C CNN
	1    -4550 8150
	0    1    1    0   
$EndComp
$Comp
L Device:C C90
U 1 1 5CDF2AA4
P -4550 8350
F 0 "C90" V -4650 8150 50  0000 C CNN
F 1 "C" V -4650 8500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -4512 8200 50  0001 C CNN
F 3 "~" H -4550 8350 50  0001 C CNN
	1    -4550 8350
	0    1    1    0   
$EndComp
$Comp
L Device:C C91
U 1 1 5CDF2AAA
P -4550 8550
F 0 "C91" V -4650 8350 50  0000 C CNN
F 1 "C" V -4650 8700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -4512 8400 50  0001 C CNN
F 3 "~" H -4550 8550 50  0001 C CNN
	1    -4550 8550
	0    1    1    0   
$EndComp
$Comp
L Device:C C92
U 1 1 5CDF2AB0
P -4550 8750
F 0 "C92" V -4650 8550 50  0000 C CNN
F 1 "C" V -4650 8900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -4512 8600 50  0001 C CNN
F 3 "~" H -4550 8750 50  0001 C CNN
	1    -4550 8750
	0    1    1    0   
$EndComp
$Comp
L Device:C C93
U 1 1 5CDF2AB6
P -4550 8950
F 0 "C93" V -4650 8750 50  0000 C CNN
F 1 "C" V -4650 9100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -4512 8800 50  0001 C CNN
F 3 "~" H -4550 8950 50  0001 C CNN
	1    -4550 8950
	0    1    1    0   
$EndComp
$Comp
L Device:C C94
U 1 1 5CDF2ABC
P -4550 9150
F 0 "C94" V -4650 8950 50  0000 C CNN
F 1 "C" V -4650 9300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -4512 9000 50  0001 C CNN
F 3 "~" H -4550 9150 50  0001 C CNN
	1    -4550 9150
	0    1    1    0   
$EndComp
$Comp
L Device:C C95
U 1 1 5CDF2AC2
P -4550 9350
F 0 "C95" V -4650 9150 50  0000 C CNN
F 1 "C" V -4650 9500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -4512 9200 50  0001 C CNN
F 3 "~" H -4550 9350 50  0001 C CNN
	1    -4550 9350
	0    1    1    0   
$EndComp
$Comp
L Device:C C96
U 1 1 5CDF2AC8
P -4550 9550
F 0 "C96" V -4650 9350 50  0000 C CNN
F 1 "C" V -4650 9700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -4512 9400 50  0001 C CNN
F 3 "~" H -4550 9550 50  0001 C CNN
	1    -4550 9550
	0    1    1    0   
$EndComp
$Comp
L Device:C C97
U 1 1 5CDF2ACE
P -3850 3350
F 0 "C97" V -3950 3150 50  0000 C CNN
F 1 "C" V -3950 3500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -3812 3200 50  0001 C CNN
F 3 "~" H -3850 3350 50  0001 C CNN
	1    -3850 3350
	0    1    1    0   
$EndComp
$Comp
L Device:C C98
U 1 1 5CDF2AD4
P -3850 3550
F 0 "C98" V -3950 3350 50  0000 C CNN
F 1 "C" V -3950 3700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -3812 3400 50  0001 C CNN
F 3 "~" H -3850 3550 50  0001 C CNN
	1    -3850 3550
	0    1    1    0   
$EndComp
$Comp
L Device:C C99
U 1 1 5CDF2ADA
P -3850 3750
F 0 "C99" V -3950 3550 50  0000 C CNN
F 1 "C" V -3950 3900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -3812 3600 50  0001 C CNN
F 3 "~" H -3850 3750 50  0001 C CNN
	1    -3850 3750
	0    1    1    0   
$EndComp
$Comp
L Device:C C100
U 1 1 5CDF2AE0
P -3850 3950
F 0 "C100" V -3950 3750 50  0000 C CNN
F 1 "C" V -3950 4100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -3812 3800 50  0001 C CNN
F 3 "~" H -3850 3950 50  0001 C CNN
	1    -3850 3950
	0    1    1    0   
$EndComp
$Comp
L Device:C C101
U 1 1 5CDF2AE6
P -3850 4150
F 0 "C101" V -3950 3950 50  0000 C CNN
F 1 "C" V -3950 4300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -3812 4000 50  0001 C CNN
F 3 "~" H -3850 4150 50  0001 C CNN
	1    -3850 4150
	0    1    1    0   
$EndComp
$Comp
L Device:C C102
U 1 1 5CDF2AEC
P -3850 4350
F 0 "C102" V -3950 4150 50  0000 C CNN
F 1 "C" V -3950 4500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -3812 4200 50  0001 C CNN
F 3 "~" H -3850 4350 50  0001 C CNN
	1    -3850 4350
	0    1    1    0   
$EndComp
$Comp
L Device:C C103
U 1 1 5CDF2AF2
P -3850 4550
F 0 "C103" V -3950 4350 50  0000 C CNN
F 1 "C" V -3950 4700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -3812 4400 50  0001 C CNN
F 3 "~" H -3850 4550 50  0001 C CNN
	1    -3850 4550
	0    1    1    0   
$EndComp
$Comp
L Device:C C104
U 1 1 5CDF2AF8
P -3850 4750
F 0 "C104" V -3950 4550 50  0000 C CNN
F 1 "C" V -3950 4900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -3812 4600 50  0001 C CNN
F 3 "~" H -3850 4750 50  0001 C CNN
	1    -3850 4750
	0    1    1    0   
$EndComp
$Comp
L Device:C C105
U 1 1 5CDF2AFE
P -3850 4950
F 0 "C105" V -3950 4750 50  0000 C CNN
F 1 "C" V -3950 5100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -3812 4800 50  0001 C CNN
F 3 "~" H -3850 4950 50  0001 C CNN
	1    -3850 4950
	0    1    1    0   
$EndComp
$Comp
L Device:C C106
U 1 1 5CDF2B04
P -3850 5150
F 0 "C106" V -3950 4950 50  0000 C CNN
F 1 "C" V -3950 5300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -3812 5000 50  0001 C CNN
F 3 "~" H -3850 5150 50  0001 C CNN
	1    -3850 5150
	0    1    1    0   
$EndComp
$Comp
L Device:C C107
U 1 1 5CDF2B0A
P -3850 5350
F 0 "C107" V -3950 5150 50  0000 C CNN
F 1 "C" V -3950 5500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -3812 5200 50  0001 C CNN
F 3 "~" H -3850 5350 50  0001 C CNN
	1    -3850 5350
	0    1    1    0   
$EndComp
$Comp
L Device:C C108
U 1 1 5CDF2B10
P -3850 5550
F 0 "C108" V -3950 5350 50  0000 C CNN
F 1 "C" V -3950 5700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -3812 5400 50  0001 C CNN
F 3 "~" H -3850 5550 50  0001 C CNN
	1    -3850 5550
	0    1    1    0   
$EndComp
$Comp
L Device:C C109
U 1 1 5CDF2B16
P -3850 5750
F 0 "C109" V -3950 5550 50  0000 C CNN
F 1 "C" V -3950 5900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -3812 5600 50  0001 C CNN
F 3 "~" H -3850 5750 50  0001 C CNN
	1    -3850 5750
	0    1    1    0   
$EndComp
$Comp
L Device:C C110
U 1 1 5CDF2B1C
P -3850 5950
F 0 "C110" V -3950 5750 50  0000 C CNN
F 1 "C" V -3950 6100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -3812 5800 50  0001 C CNN
F 3 "~" H -3850 5950 50  0001 C CNN
	1    -3850 5950
	0    1    1    0   
$EndComp
$Comp
L Device:C C111
U 1 1 5CDF2B22
P -3850 6150
F 0 "C111" V -3950 5950 50  0000 C CNN
F 1 "C" V -3950 6300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -3812 6000 50  0001 C CNN
F 3 "~" H -3850 6150 50  0001 C CNN
	1    -3850 6150
	0    1    1    0   
$EndComp
$Comp
L Device:C C112
U 1 1 5CDF2B28
P -3850 6350
F 0 "C112" V -3950 6150 50  0000 C CNN
F 1 "C" V -3950 6500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -3812 6200 50  0001 C CNN
F 3 "~" H -3850 6350 50  0001 C CNN
	1    -3850 6350
	0    1    1    0   
$EndComp
$Comp
L Device:C C113
U 1 1 5CDF2B2E
P -3850 6550
F 0 "C113" V -3950 6350 50  0000 C CNN
F 1 "C" V -3950 6700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -3812 6400 50  0001 C CNN
F 3 "~" H -3850 6550 50  0001 C CNN
	1    -3850 6550
	0    1    1    0   
$EndComp
$Comp
L Device:C C114
U 1 1 5CDF2B34
P -3850 6750
F 0 "C114" V -3950 6550 50  0000 C CNN
F 1 "C" V -3950 6900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -3812 6600 50  0001 C CNN
F 3 "~" H -3850 6750 50  0001 C CNN
	1    -3850 6750
	0    1    1    0   
$EndComp
$Comp
L Device:C C115
U 1 1 5CDF2B3A
P -3850 6950
F 0 "C115" V -3950 6750 50  0000 C CNN
F 1 "C" V -3950 7100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -3812 6800 50  0001 C CNN
F 3 "~" H -3850 6950 50  0001 C CNN
	1    -3850 6950
	0    1    1    0   
$EndComp
$Comp
L Device:C C116
U 1 1 5CDF2B40
P -3850 7150
F 0 "C116" V -3950 6950 50  0000 C CNN
F 1 "C" V -3950 7300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -3812 7000 50  0001 C CNN
F 3 "~" H -3850 7150 50  0001 C CNN
	1    -3850 7150
	0    1    1    0   
$EndComp
$Comp
L Device:C C117
U 1 1 5CDF2B46
P -3850 7350
F 0 "C117" V -3950 7150 50  0000 C CNN
F 1 "C" V -3950 7500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -3812 7200 50  0001 C CNN
F 3 "~" H -3850 7350 50  0001 C CNN
	1    -3850 7350
	0    1    1    0   
$EndComp
$Comp
L Device:C C118
U 1 1 5CDF2B4C
P -3850 7550
F 0 "C118" V -3950 7350 50  0000 C CNN
F 1 "C" V -3950 7700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -3812 7400 50  0001 C CNN
F 3 "~" H -3850 7550 50  0001 C CNN
	1    -3850 7550
	0    1    1    0   
$EndComp
$Comp
L Device:C C119
U 1 1 5CDF2B52
P -3850 7750
F 0 "C119" V -3950 7550 50  0000 C CNN
F 1 "C" V -3950 7900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -3812 7600 50  0001 C CNN
F 3 "~" H -3850 7750 50  0001 C CNN
	1    -3850 7750
	0    1    1    0   
$EndComp
$Comp
L Device:C C120
U 1 1 5CDF2B58
P -3850 7950
F 0 "C120" V -3950 7750 50  0000 C CNN
F 1 "C" V -3950 8100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -3812 7800 50  0001 C CNN
F 3 "~" H -3850 7950 50  0001 C CNN
	1    -3850 7950
	0    1    1    0   
$EndComp
$Comp
L Device:C C121
U 1 1 5CDF2B5E
P -3850 8150
F 0 "C121" V -3950 7950 50  0000 C CNN
F 1 "C" V -3950 8300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -3812 8000 50  0001 C CNN
F 3 "~" H -3850 8150 50  0001 C CNN
	1    -3850 8150
	0    1    1    0   
$EndComp
$Comp
L Device:C C122
U 1 1 5CDF2B64
P -3850 8350
F 0 "C122" V -3950 8150 50  0000 C CNN
F 1 "C" V -3950 8500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -3812 8200 50  0001 C CNN
F 3 "~" H -3850 8350 50  0001 C CNN
	1    -3850 8350
	0    1    1    0   
$EndComp
$Comp
L Device:C C123
U 1 1 5CDF2B6A
P -3850 8550
F 0 "C123" V -3950 8350 50  0000 C CNN
F 1 "C" V -3950 8700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -3812 8400 50  0001 C CNN
F 3 "~" H -3850 8550 50  0001 C CNN
	1    -3850 8550
	0    1    1    0   
$EndComp
$Comp
L Device:C C124
U 1 1 5CDF2B70
P -3850 8750
F 0 "C124" V -3950 8550 50  0000 C CNN
F 1 "C" V -3950 8900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -3812 8600 50  0001 C CNN
F 3 "~" H -3850 8750 50  0001 C CNN
	1    -3850 8750
	0    1    1    0   
$EndComp
$Comp
L Device:C C125
U 1 1 5CDF2B76
P -3850 8950
F 0 "C125" V -3950 8750 50  0000 C CNN
F 1 "C" V -3950 9100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -3812 8800 50  0001 C CNN
F 3 "~" H -3850 8950 50  0001 C CNN
	1    -3850 8950
	0    1    1    0   
$EndComp
$Comp
L Device:C C126
U 1 1 5CDF2B7C
P -3850 9150
F 0 "C126" V -3950 8950 50  0000 C CNN
F 1 "C" V -3950 9300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -3812 9000 50  0001 C CNN
F 3 "~" H -3850 9150 50  0001 C CNN
	1    -3850 9150
	0    1    1    0   
$EndComp
$Comp
L Device:C C127
U 1 1 5CDF2B82
P -3850 9350
F 0 "C127" V -3950 9150 50  0000 C CNN
F 1 "C" V -3950 9500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -3812 9200 50  0001 C CNN
F 3 "~" H -3850 9350 50  0001 C CNN
	1    -3850 9350
	0    1    1    0   
$EndComp
$Comp
L Device:C C128
U 1 1 5CDF2B88
P -3850 9550
F 0 "C128" V -3950 9350 50  0000 C CNN
F 1 "C" V -3950 9700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H -3812 9400 50  0001 C CNN
F 3 "~" H -3850 9550 50  0001 C CNN
	1    -3850 9550
	0    1    1    0   
$EndComp
Wire Wire Line
	-5850 9700 -5850 9550
Connection ~ -5850 3550
Wire Wire Line
	-5850 3550 -5850 3350
Connection ~ -5850 3750
Wire Wire Line
	-5850 3750 -5850 3550
Connection ~ -5850 3950
Wire Wire Line
	-5850 3950 -5850 3750
Connection ~ -5850 4150
Wire Wire Line
	-5850 4150 -5850 3950
Connection ~ -5850 4350
Wire Wire Line
	-5850 4350 -5850 4150
Connection ~ -5850 4550
Wire Wire Line
	-5850 4550 -5850 4350
Connection ~ -5850 4750
Wire Wire Line
	-5850 4750 -5850 4550
Connection ~ -5850 4950
Wire Wire Line
	-5850 4950 -5850 4750
Connection ~ -5850 5150
Wire Wire Line
	-5850 5150 -5850 4950
Connection ~ -5850 5350
Wire Wire Line
	-5850 5350 -5850 5150
Connection ~ -5850 5550
Wire Wire Line
	-5850 5550 -5850 5350
Connection ~ -5850 5750
Wire Wire Line
	-5850 5750 -5850 5550
Connection ~ -5850 5950
Wire Wire Line
	-5850 5950 -5850 5750
Connection ~ -5850 6150
Wire Wire Line
	-5850 6150 -5850 5950
Connection ~ -5850 6350
Wire Wire Line
	-5850 6350 -5850 6150
Connection ~ -5850 6550
Wire Wire Line
	-5850 6550 -5850 6350
Connection ~ -5850 6750
Wire Wire Line
	-5850 6750 -5850 6550
Connection ~ -5850 6950
Wire Wire Line
	-5850 6950 -5850 6750
Connection ~ -5850 7150
Wire Wire Line
	-5850 7150 -5850 6950
Connection ~ -5850 7350
Wire Wire Line
	-5850 7350 -5850 7150
Connection ~ -5850 7550
Wire Wire Line
	-5850 7550 -5850 7350
Connection ~ -5850 7750
Wire Wire Line
	-5850 7750 -5850 7550
Connection ~ -5850 7950
Wire Wire Line
	-5850 7950 -5850 7750
Connection ~ -5850 8150
Wire Wire Line
	-5850 8150 -5850 7950
Connection ~ -5850 8350
Wire Wire Line
	-5850 8350 -5850 8150
Connection ~ -5850 8550
Wire Wire Line
	-5850 8550 -5850 8350
Connection ~ -5850 8750
Wire Wire Line
	-5850 8750 -5850 8550
Connection ~ -5850 8950
Wire Wire Line
	-5850 8950 -5850 8750
Connection ~ -5850 9150
Wire Wire Line
	-5850 9150 -5850 8950
Connection ~ -5850 9350
Wire Wire Line
	-5850 9350 -5850 9150
Connection ~ -5850 9550
Wire Wire Line
	-5850 9550 -5850 9350
Wire Wire Line
	-5150 9700 -5150 9550
Connection ~ -5150 3550
Wire Wire Line
	-5150 3550 -5150 3350
Connection ~ -5150 3750
Wire Wire Line
	-5150 3750 -5150 3550
Connection ~ -5150 3950
Wire Wire Line
	-5150 3950 -5150 3750
Connection ~ -5150 4150
Wire Wire Line
	-5150 4150 -5150 3950
Connection ~ -5150 4350
Wire Wire Line
	-5150 4350 -5150 4150
Connection ~ -5150 4550
Wire Wire Line
	-5150 4550 -5150 4350
Connection ~ -5150 4750
Wire Wire Line
	-5150 4750 -5150 4550
Connection ~ -5150 4950
Wire Wire Line
	-5150 4950 -5150 4750
Connection ~ -5150 5150
Wire Wire Line
	-5150 5150 -5150 4950
Connection ~ -5150 5350
Wire Wire Line
	-5150 5350 -5150 5150
Connection ~ -5150 5550
Wire Wire Line
	-5150 5550 -5150 5350
Connection ~ -5150 5750
Wire Wire Line
	-5150 5750 -5150 5550
Connection ~ -5150 5950
Wire Wire Line
	-5150 5950 -5150 5750
Connection ~ -5150 6150
Wire Wire Line
	-5150 6150 -5150 5950
Connection ~ -5150 6350
Wire Wire Line
	-5150 6350 -5150 6150
Connection ~ -5150 6550
Wire Wire Line
	-5150 6550 -5150 6350
Connection ~ -5150 6750
Wire Wire Line
	-5150 6750 -5150 6550
Connection ~ -5150 6950
Wire Wire Line
	-5150 6950 -5150 6750
Connection ~ -5150 7150
Wire Wire Line
	-5150 7150 -5150 6950
Connection ~ -5150 7350
Wire Wire Line
	-5150 7350 -5150 7150
Connection ~ -5150 7550
Wire Wire Line
	-5150 7550 -5150 7350
Connection ~ -5150 7750
Wire Wire Line
	-5150 7750 -5150 7550
Connection ~ -5150 7950
Wire Wire Line
	-5150 7950 -5150 7750
Connection ~ -5150 8150
Wire Wire Line
	-5150 8150 -5150 7950
Connection ~ -5150 8350
Wire Wire Line
	-5150 8350 -5150 8150
Connection ~ -5150 8550
Wire Wire Line
	-5150 8550 -5150 8350
Connection ~ -5150 8750
Wire Wire Line
	-5150 8750 -5150 8550
Connection ~ -5150 8950
Wire Wire Line
	-5150 8950 -5150 8750
Connection ~ -5150 9150
Wire Wire Line
	-5150 9150 -5150 8950
Connection ~ -5150 9350
Wire Wire Line
	-5150 9350 -5150 9150
Connection ~ -5150 9550
Wire Wire Line
	-5150 9550 -5150 9350
Wire Wire Line
	-4400 3350 -4400 3550
Connection ~ -4400 3550
Wire Wire Line
	-4400 3550 -4400 3750
Connection ~ -4400 3750
Wire Wire Line
	-4400 3750 -4400 3950
Connection ~ -4400 3950
Wire Wire Line
	-4400 3950 -4400 4150
Connection ~ -4400 4150
Wire Wire Line
	-4400 4150 -4400 4350
Connection ~ -4400 4350
Wire Wire Line
	-4400 4350 -4400 4550
Connection ~ -4400 4550
Wire Wire Line
	-4400 4550 -4400 4750
Connection ~ -4400 4750
Wire Wire Line
	-4400 4750 -4400 4950
Connection ~ -4400 4950
Wire Wire Line
	-4400 4950 -4400 5150
Connection ~ -4400 5150
Wire Wire Line
	-4400 5150 -4400 5350
Connection ~ -4400 5350
Wire Wire Line
	-4400 5350 -4400 5550
Connection ~ -4400 5550
Wire Wire Line
	-4400 5550 -4400 5750
Connection ~ -4400 5750
Wire Wire Line
	-4400 5750 -4400 5950
Connection ~ -4400 5950
Wire Wire Line
	-4400 5950 -4400 6150
Connection ~ -4400 6150
Wire Wire Line
	-4400 6150 -4400 6350
Connection ~ -4400 6350
Wire Wire Line
	-4400 6350 -4400 6550
Connection ~ -4400 6550
Wire Wire Line
	-4400 6550 -4400 6750
Connection ~ -4400 6750
Wire Wire Line
	-4400 6750 -4400 6950
Connection ~ -4400 6950
Wire Wire Line
	-4400 6950 -4400 7150
Connection ~ -4400 7150
Wire Wire Line
	-4400 7150 -4400 7350
Connection ~ -4400 7350
Wire Wire Line
	-4400 7350 -4400 7550
Connection ~ -4400 7550
Wire Wire Line
	-4400 7550 -4400 7750
Connection ~ -4400 7750
Wire Wire Line
	-4400 7750 -4400 7950
Connection ~ -4400 7950
Wire Wire Line
	-4400 7950 -4400 8150
Connection ~ -4400 8150
Wire Wire Line
	-4400 8150 -4400 8350
Connection ~ -4400 8350
Wire Wire Line
	-4400 8350 -4400 8550
Connection ~ -4400 8550
Wire Wire Line
	-4400 8550 -4400 8750
Connection ~ -4400 8750
Wire Wire Line
	-4400 8750 -4400 8950
Connection ~ -4400 8950
Wire Wire Line
	-4400 8950 -4400 9150
Connection ~ -4400 9150
Wire Wire Line
	-4400 9150 -4400 9350
Connection ~ -4400 9350
Wire Wire Line
	-4400 9350 -4400 9550
Connection ~ -4400 9550
Wire Wire Line
	-4400 9550 -4400 9700
Wire Wire Line
	-3700 9700 -3700 9550
Connection ~ -3700 3550
Wire Wire Line
	-3700 3550 -3700 3350
Connection ~ -3700 3750
Wire Wire Line
	-3700 3750 -3700 3550
Connection ~ -3700 3950
Wire Wire Line
	-3700 3950 -3700 3750
Connection ~ -3700 4150
Wire Wire Line
	-3700 4150 -3700 3950
Connection ~ -3700 4350
Wire Wire Line
	-3700 4350 -3700 4150
Connection ~ -3700 4550
Wire Wire Line
	-3700 4550 -3700 4350
Connection ~ -3700 4750
Wire Wire Line
	-3700 4750 -3700 4550
Connection ~ -3700 4950
Wire Wire Line
	-3700 4950 -3700 4750
Connection ~ -3700 5150
Wire Wire Line
	-3700 5150 -3700 4950
Connection ~ -3700 5350
Wire Wire Line
	-3700 5350 -3700 5150
Connection ~ -3700 5550
Wire Wire Line
	-3700 5550 -3700 5350
Connection ~ -3700 5750
Wire Wire Line
	-3700 5750 -3700 5550
Connection ~ -3700 5950
Wire Wire Line
	-3700 5950 -3700 5750
Connection ~ -3700 6150
Wire Wire Line
	-3700 6150 -3700 5950
Connection ~ -3700 6350
Wire Wire Line
	-3700 6350 -3700 6150
Connection ~ -3700 6550
Wire Wire Line
	-3700 6550 -3700 6350
Connection ~ -3700 6750
Wire Wire Line
	-3700 6750 -3700 6550
Connection ~ -3700 6950
Wire Wire Line
	-3700 6950 -3700 6750
Connection ~ -3700 7150
Wire Wire Line
	-3700 7150 -3700 6950
Connection ~ -3700 7350
Wire Wire Line
	-3700 7350 -3700 7150
Connection ~ -3700 7550
Wire Wire Line
	-3700 7550 -3700 7350
Connection ~ -3700 7750
Wire Wire Line
	-3700 7750 -3700 7550
Connection ~ -3700 7950
Wire Wire Line
	-3700 7950 -3700 7750
Connection ~ -3700 8150
Wire Wire Line
	-3700 8150 -3700 7950
Connection ~ -3700 8350
Wire Wire Line
	-3700 8350 -3700 8150
Connection ~ -3700 8550
Wire Wire Line
	-3700 8550 -3700 8350
Connection ~ -3700 8750
Wire Wire Line
	-3700 8750 -3700 8550
Connection ~ -3700 8950
Wire Wire Line
	-3700 8950 -3700 8750
Connection ~ -3700 9150
Wire Wire Line
	-3700 9150 -3700 8950
Connection ~ -3700 9350
Wire Wire Line
	-3700 9350 -3700 9150
Connection ~ -3700 9550
Wire Wire Line
	-3700 9550 -3700 9350
Wire Wire Line
	-5850 9700 -5150 9700
Connection ~ -5150 9700
Wire Wire Line
	-5150 9700 -4400 9700
Connection ~ -4400 9700
Wire Wire Line
	-4400 9700 -3700 9700
Wire Wire Line
	-3700 9700 -3700 9800
Connection ~ -3700 9700
$Comp
L power:Earth #PWR0101
U 1 1 5CE0D508
P -3700 9800
F 0 "#PWR0101" H -3700 9550 50  0001 C CNN
F 1 "Earth" H -3700 9650 50  0001 C CNN
F 2 "" H -3700 9800 50  0001 C CNN
F 3 "~" H -3700 9800 50  0001 C CNN
	1    -3700 9800
	1    0    0    -1  
$EndComp
Text GLabel -6150 3350 0    50   Input ~ 0
1
Text GLabel -6150 3550 0    50   Input ~ 0
2
Text GLabel -6150 3750 0    50   Input ~ 0
3
Text GLabel -6150 3950 0    50   Input ~ 0
4
Text GLabel -6150 4150 0    50   Input ~ 0
5
Text GLabel -6150 4350 0    50   Input ~ 0
6
Text GLabel -6150 4550 0    50   Input ~ 0
7
Text GLabel -6150 4750 0    50   Input ~ 0
8
Text GLabel -6150 4950 0    50   Input ~ 0
9
Text GLabel -6150 5150 0    50   Input ~ 0
10
Text GLabel -6150 5350 0    50   Input ~ 0
11
Text GLabel -6150 5550 0    50   Input ~ 0
12
Text GLabel -6150 5750 0    50   Input ~ 0
13
Text GLabel -6150 5950 0    50   Input ~ 0
14
Text GLabel -6150 6150 0    50   Input ~ 0
15
Text GLabel -6150 6350 0    50   Input ~ 0
16
Text GLabel -6150 6550 0    50   Input ~ 0
17
Text GLabel -6150 6750 0    50   Input ~ 0
18
Text GLabel -6150 6950 0    50   Input ~ 0
19
Text GLabel -6150 7150 0    50   Input ~ 0
20
Text GLabel -6150 7350 0    50   Input ~ 0
21
Text GLabel -6150 7550 0    50   Input ~ 0
22
Text GLabel -6150 7750 0    50   Input ~ 0
23
Text GLabel -6150 7950 0    50   Input ~ 0
24
Text GLabel -6150 8150 0    50   Input ~ 0
25
Text GLabel -6150 8350 0    50   Input ~ 0
26
Text GLabel -6150 8550 0    50   Input ~ 0
27
Text GLabel -6150 8750 0    50   Input ~ 0
28
Text GLabel -6150 8950 0    50   Input ~ 0
29
Text GLabel -6150 9150 0    50   Input ~ 0
30
Text GLabel -6150 9350 0    50   Input ~ 0
31
Text GLabel -6150 9550 0    50   Input ~ 0
32
Text GLabel -5450 9550 0    50   Input ~ 0
64
Text GLabel -5450 9350 0    50   Input ~ 0
63
Text GLabel -5450 9150 0    50   Input ~ 0
62
Text GLabel -5450 8950 0    50   Input ~ 0
61
Text GLabel -5450 8750 0    50   Input ~ 0
60
Text GLabel -5450 8550 0    50   Input ~ 0
59
Text GLabel -5450 8350 0    50   Input ~ 0
58
Text GLabel -5450 8150 0    50   Input ~ 0
57
Text GLabel -5450 7950 0    50   Input ~ 0
56
Text GLabel -5450 7750 0    50   Input ~ 0
55
Text GLabel -5450 7550 0    50   Input ~ 0
54
Text GLabel -5450 7350 0    50   Input ~ 0
53
Text GLabel -5450 7150 0    50   Input ~ 0
52
Text GLabel -5450 6950 0    50   Input ~ 0
51
Text GLabel -5450 6750 0    50   Input ~ 0
50
Text GLabel -5450 6550 0    50   Input ~ 0
49
Text GLabel -5450 6350 0    50   Input ~ 0
48
Text GLabel -5450 6150 0    50   Input ~ 0
47
Text GLabel -5450 5950 0    50   Input ~ 0
46
Text GLabel -5450 5750 0    50   Input ~ 0
45
Text GLabel -5450 5550 0    50   Input ~ 0
44
Text GLabel -5450 5350 0    50   Input ~ 0
43
Text GLabel -5450 5150 0    50   Input ~ 0
42
Text GLabel -5450 4950 0    50   Input ~ 0
41
Text GLabel -5450 4750 0    50   Input ~ 0
40
Text GLabel -5450 4550 0    50   Input ~ 0
39
Text GLabel -5450 4350 0    50   Input ~ 0
38
Text GLabel -5450 4150 0    50   Input ~ 0
37
Text GLabel -5450 3950 0    50   Input ~ 0
36
Text GLabel -5450 3750 0    50   Input ~ 0
35
Text GLabel -5450 3550 0    50   Input ~ 0
34
Text GLabel -5450 3350 0    50   Input ~ 0
33
Text GLabel -4700 3350 0    50   Input ~ 0
65
Text GLabel -4700 3550 0    50   Input ~ 0
66
Text GLabel -4700 3750 0    50   Input ~ 0
67
Text GLabel -4700 3950 0    50   Input ~ 0
68
Text GLabel -4700 4150 0    50   Input ~ 0
69
Text GLabel -4700 4350 0    50   Input ~ 0
70
Text GLabel -4700 4550 0    50   Input ~ 0
71
Text GLabel -4700 4750 0    50   Input ~ 0
72
Text GLabel -4700 4950 0    50   Input ~ 0
73
Text GLabel -4700 5150 0    50   Input ~ 0
74
Text GLabel -4700 5350 0    50   Input ~ 0
75
Text GLabel -4700 5550 0    50   Input ~ 0
76
Text GLabel -4700 5750 0    50   Input ~ 0
77
Text GLabel -4700 5950 0    50   Input ~ 0
78
Text GLabel -4700 6150 0    50   Input ~ 0
79
Text GLabel -4700 6350 0    50   Input ~ 0
80
Text GLabel -4700 6550 0    50   Input ~ 0
81
Text GLabel -4700 6750 0    50   Input ~ 0
82
Text GLabel -4700 6950 0    50   Input ~ 0
83
Text GLabel -4700 7150 0    50   Input ~ 0
84
Text GLabel -4700 7350 0    50   Input ~ 0
85
Text GLabel -4700 7550 0    50   Input ~ 0
86
Text GLabel -4700 7750 0    50   Input ~ 0
87
Text GLabel -4700 7950 0    50   Input ~ 0
88
Text GLabel -4700 8150 0    50   Input ~ 0
89
Text GLabel -4700 8350 0    50   Input ~ 0
90
Text GLabel -4700 8550 0    50   Input ~ 0
91
Text GLabel -4700 8750 0    50   Input ~ 0
92
Text GLabel -4700 8950 0    50   Input ~ 0
93
Text GLabel -4700 9150 0    50   Input ~ 0
94
Text GLabel -4700 9350 0    50   Input ~ 0
95
Text GLabel -4700 9550 0    50   Input ~ 0
96
Text GLabel -4000 9550 0    50   Input ~ 0
128
Text GLabel -4000 9350 0    50   Input ~ 0
127
Text GLabel -4000 9150 0    50   Input ~ 0
126
Text GLabel -4000 8950 0    50   Input ~ 0
125
Text GLabel -4000 8750 0    50   Input ~ 0
124
Text GLabel -4000 8550 0    50   Input ~ 0
123
Text GLabel -4000 8350 0    50   Input ~ 0
122
Text GLabel -4000 8150 0    50   Input ~ 0
121
Text GLabel -4000 7950 0    50   Input ~ 0
120
Text GLabel -4000 7750 0    50   Input ~ 0
119
Text GLabel -4000 7550 0    50   Input ~ 0
118
Text GLabel -4000 7350 0    50   Input ~ 0
117
Text GLabel -4000 7150 0    50   Input ~ 0
116
Text GLabel -4000 6950 0    50   Input ~ 0
115
Text GLabel -4000 6750 0    50   Input ~ 0
114
Text GLabel -4000 6550 0    50   Input ~ 0
113
Text GLabel -4000 6350 0    50   Input ~ 0
112
Text GLabel -4000 6150 0    50   Input ~ 0
111
Text GLabel -4000 5950 0    50   Input ~ 0
110
Text GLabel -4000 5750 0    50   Input ~ 0
109
Text GLabel -4000 5550 0    50   Input ~ 0
108
Text GLabel -4000 5350 0    50   Input ~ 0
107
Text GLabel -4000 5150 0    50   Input ~ 0
106
Text GLabel -4000 4950 0    50   Input ~ 0
105
Text GLabel -4000 4750 0    50   Input ~ 0
104
Text GLabel -4000 4550 0    50   Input ~ 0
103
Text GLabel -4000 4350 0    50   Input ~ 0
102
Text GLabel -4000 4150 0    50   Input ~ 0
101
Text GLabel -4000 3950 0    50   Input ~ 0
100
Text GLabel -4000 3750 0    50   Input ~ 0
99
Text GLabel -4000 3550 0    50   Input ~ 0
98
Text GLabel -4000 3350 0    50   Input ~ 0
97
$Comp
L TQFP_BREAKOUT_BOARD:TQFP_BREAKOUT_BOARD U2
U 1 1 5CD69CF4
P 8300 2950
F 0 "U2" H 8250 2950 50  0000 L CNN
F 1 "TQFP_BREAKOUT_BOARD" H 7850 2850 50  0000 L CNN
F 2 "Package_QFP:LQFP-128_14x14mm_P0.4mm" H 8300 2950 50  0001 C CNN
F 3 "" H 8300 2950 50  0001 C CNN
	1    8300 2950
	1    0    0    -1  
$EndComp
$EndSCHEMATC
